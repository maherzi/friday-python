import requests
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import io

period = 15

def query(stock):
    key='2S0WGVUJIFWW3QTU'

    req = requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY_EXTENDED&symbol={}&interval=1min&apikey={}'.format(stock, key))
    return req.text



def createDf(csvString):
    df = pd.read_csv(io.StringIO(csvString))
    df['time'] = pd.to_datetime(df['time'])
    return df

def filterByTime(df, timeString):
    start_date = '2020-09-03 07:15:00'
    end_date = '2020-09-03 {}:00'.format(timeString)
    return df[ (df['time'] >= start_date) & (df['time'] <= end_date)]


def determineBuySell(df):
    avgClose = df.iloc[period:]['close'].rolling(window=period).mean().dropna()
    stdClose = df.iloc[period:]['close'].rolling(window=period).std().dropna()

    shortClose = df[period*2-1:]['close']#.rolling(4).mean()

    upperBound = avgClose + (2 * stdClose)
    lowerBound = avgClose - (2 * stdClose)

    currentAvgClose = avgClose.iloc[0]
    currentClose = shortClose.iloc[0]

    ratio_amount = 1.0

    currentAvgClose = avgClose.iloc[0]
    currentClose = shortClose.iloc[0]
    print(currentClose)
    print(currentAvgClose)
    ratio = currentClose/currentAvgClose
    if currentClose > currentAvgClose:
        if ratio > ratio_amount:
            return "sell"
    elif currentClose > currentAvgClose:
        if ratio > ratio_amount:
            return "buy"
    else:
        return "hold"

def run(stockTicker, timeString):
    data = query(stockTicker)
    df = createDf(data)
    time_df = filterByTime(df, timeString)
    return determineBuySell(time_df)


if __name__ == '__main__':
    print("hello")
    run('AAPL','13:59')




