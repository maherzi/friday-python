from flask import Flask, render_template, request, jsonify
from pricecalc import run
app = Flask(__name__)


@app.route('/')
def hello():
    return render_template('home.html')

@app.route('/check', methods=['GET'])
def determine():
    ticker = request.args.get('ticker')
    time = request.args.get('time')

    val = run(ticker, time)
    print(val)

    return jsonify(val)

if __name__ == '__main__':
    app.run(debug=True)
